package nl.rubix.api.contractfirst.rest;

import org.apache.camel.builder.RouteBuilder;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A bean which we use in the route
 */
public class APIRouteBuilder extends RouteBuilder{

    @Override
    public void configure() throws Exception {
        from("cxfrs:bean:api?bindingStyle=SimpleConsumer").log("you did it!");

    }
}
